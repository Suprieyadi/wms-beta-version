____________________________________________________
/__________________________________________________/
							  	
██╗    ██╗███╗   ███╗███████╗ dhian.utama@gmail.com
██║    ██║████╗ ████║██╔════╝ pacay91@gmail.com
██║ █╗ ██║██╔████╔██║███████╗ weersyohan@gmail.com
██║███╗██║██║╚██╔╝██║╚════██║ suprieyadi@gmail.com
╚███╔███╔╝██║ ╚═╝ ██║███████║
 ╚══╝╚══╝ ╚═╝     ╚═╝╚══════╝ 
 
 Warehouse Management System
____________________________________________________
/__________________________________________________/    

## Getting Started

1. Change data source(MYSQL) url in application.yml and create your database without tables (will be generated automatically)
1. Install dependencies
1. `mvn spring-boot:run` to run application
1. Browse http://localhost:9090

## [Wiki](https://github.com/bndynet/web-framework-for-java/wiki)

- [Build your website](https://github.com/bndynet/web-framework-for-java/wiki/Build-your-website)
- [Development](https://github.com/bndynet/web-framework-for-java/wiki/Development)
- [Implemented Features](https://github.com/bndynet/web-framework-for-java/wiki/Implemented-Features)
- [OAuth Endpoints](https://github.com/bndynet/web-framework-for-java/wiki/OAuth-Endpoints)
- [Jenkins CI](https://github.com/bndynet/web-framework-for-java/wiki/Jenkins-CI)

## Development
# Locations
src/main/java/com/wms/wf/modules: Java Modules (services and RESTful API)
src/main/java/com/wms/wf/controller: Controllers
src/main/resources/templates: Thymeleaf Modules
src/main/resources/public/docs/api: Auto-generated API Documentation (Swagger UI)
src/main/resources/static/apps/admin: AngularJS Application for Admin Panel
src/main/resources/static/apps/admin/modules: AngularJS Modules
src/main/resources/static/apps/admin/modules/example: Style Examples

# Available Frontend Components
ui-actions, ui-dialog, ui-html-editor, ui-input, ui-menu-tree, ui-no-data, ui-notifications, ui-page-content, ui-page-header, ui-search, ui-tree, ui-upload, ui-wait-on

# Recommendations
- Non-Frontend

You just use thymeleaf and not angularjs, you can define controllers in src/main/java/com/wms/wf/controller and thymeleaf templates in src/main/resources/templates.

- AngularJS

You can implement api in src/main/java/com/wms/wf/modules and angularjs controllers in src/main/resources/static/apps/admin/modules. Each feature should include one html file and one js file.

- Call API (appService service)

[ajaxGet|ajaxSave|ajaxDelete]('/api/...'[, data]) to request resources, ajaxSave instead of ajaxPut and ajaxPost which will call ajaxPut or ajaxPost according to data.id.

- Dialog & Notification (appDialog service)

[loading|wait|clearWait|success|info|warning|error|alert|confirm|confirmDeletion|show|showWin]

- International (Languages)

You can define both of backend languages and frontend languages in src/main/resources/i18n/. And you can use it in AngularJS modules directly.

Examples:

welcome.message=Hi, {0}! in i18n/messages.properties

<!-- call in AngularJS -->
<span ng-bind="{welcome.message|translate:[name]}"></span>

<!-- call in Thymelefa -->
<span th:text="#{welcome.message(${name})}"></span>