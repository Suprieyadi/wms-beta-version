/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoResourceFoundException extends AppException {
    private static final long serialVersionUID = 1L;

    public NoResourceFoundException() {
        super("error.noResourceFound");
    }

    public NoResourceFoundException(String resourceName) {
        super(resourceName);
    }
}