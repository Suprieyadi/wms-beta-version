/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.exceptions;

import org.springframework.http.HttpStatus;

import com.wms.wf.lib.ResponseResult;

public class ApiError extends ResponseResult<Exception> {

	public ApiError(Exception data) {
		super(data);
		this.setStatus(HttpStatus.BAD_REQUEST);
		this.setMessage(data.getMessage());
	}
}
