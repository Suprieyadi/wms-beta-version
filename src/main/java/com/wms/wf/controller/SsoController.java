/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.wms.wf.exceptions.AbnormalAccountException;
import com.wms.wf.modules.core.models.User;
import com.wms.wf.modules.core.services.SecurityService;
import com.wms.wf.modules.core.services.UserService;
import com.wms.wf.modules.core.services.UserValidator;

@Controller
@RequestMapping("/sso")
public class SsoController extends _BaseController {

	@Autowired
	private UserService userService;
	@Autowired
	private UserValidator userValidator;
	@Autowired
	private SecurityService securityService;

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup(Model model, HttpSession session) {
		model.addAttribute("model", new User());
		if(!this.userService.hasUsers()) {
			model.addAttribute("admin", true);
		}
		return "sso/signup";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String signup(@ModelAttribute("model") User user, BindingResult bindingResult, Model model) throws AbnormalAccountException {
		userValidator.validate(user, bindingResult);

		if (bindingResult.hasErrors()) {
			return "sso/signup";
		}

		user = userService.save(user);
		
		if (!user.isEnabled()) {
			throw new AbnormalAccountException("error.disabledUser");
		}
		if (user.isLocked()) {
			throw new AbnormalAccountException("error.lockedUser");
		}
		if (user.isExpired()) {
			throw new AbnormalAccountException("error.expiredUser");
		}

		securityService.autologin(user.getUsername(), user.getPasswordConfirm());

		return "redirect:/";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model, String error, String logout, String redirect_uri, HttpSession session) {
		// create first user
		if(!this.userService.hasUsers()) {
			return "redirect:/sso/signup";
		}
		
		return "sso/login";
	}
}
