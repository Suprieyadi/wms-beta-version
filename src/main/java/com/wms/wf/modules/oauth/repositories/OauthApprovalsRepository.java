/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.modules.oauth.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wms.wf.modules.oauth.models.OauthApprovals;

import java.util.Collection;

public interface OauthApprovalsRepository extends JpaRepository<OauthApprovals, Long> {

    OauthApprovals findByUserIdAndClientId(String userId, String clientId);

    Collection<OauthApprovals> findByUserId(String userId);
}
