/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.modules.oauth.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wms.wf.modules.oauth.models.OauthClientDetails;

public interface OauthClientDetailsRepository extends JpaRepository<OauthClientDetails, Long> {

    OauthClientDetails findByClientId(String clientId);
}
