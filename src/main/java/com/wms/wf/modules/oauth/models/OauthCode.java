/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.modules.oauth.models;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.wms.wf.lib._BaseEntity;

import java.sql.Blob;

@Entity
@Table(name = "oauth_code")
public class OauthCode extends _BaseEntity {
	private static final long serialVersionUID = 1L;

	private String code;
    private Blob authentication;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Blob getAuthentication() {
		return authentication;
	}
	public void setAuthentication(Blob authentication) {
		this.authentication = authentication;
	}
}
