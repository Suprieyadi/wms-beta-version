/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.modules.oauth.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.approval.Approval;
import org.springframework.stereotype.Service;

import com.wms.wf.lib._BaseService;
import com.wms.wf.modules.oauth.models.OauthApprovals;
import com.wms.wf.modules.oauth.models.OauthClientDetails;
import com.wms.wf.modules.oauth.repositories.OauthApprovalsRepository;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
@Transactional
public class OauthService extends _BaseService<OauthClientDetails> {

    @Autowired
    private OauthApprovalsRepository oauthApprovalsRepository;

    public boolean isApprovedClient(String clientId, String username) {
        OauthApprovals oa = this.oauthApprovalsRepository.findByUserIdAndClientId(username, clientId);
        if (oa != null && Approval.ApprovalStatus.valueOf(oa.getStatus()) == Approval.ApprovalStatus.APPROVED) {
            return true;
        }
        return false;
    }

    public Collection<OauthApprovals> getApprovalsByUser(String username) {
        return this.oauthApprovalsRepository.findByUserId(username);
    }
}
