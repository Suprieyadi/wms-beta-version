package com.wms.wf.modules.core.api;

import io.swagger.annotations.Api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wms.wf.lib._BaseApi;
import com.wms.wf.modules.core.models.File;

@Api(value = "File Resource API")
@RestController
@RequestMapping({"/api/core/files"})
public class FileController  extends _BaseApi<File> {

}
