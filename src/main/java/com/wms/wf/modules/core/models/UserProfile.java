/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.modules.core.models;

import java.util.Date;
import javax.persistence.*;

import com.wms.wf.lib._BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@Table(name="core_user_profile")
public class UserProfile extends _BaseEntity {
	private static final long serialVersionUID = 1L;

	@Column(columnDefinition = "BIGINT UNSIGNED", unique = true, nullable = false)
	private Long userId;
	private String email;
	private String realName;
	private String gender;
	@Column(columnDefinition = "DATE")
	private Date birthday;
	@OneToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "user_id", insertable = false, updatable = false)
	private User user;
	
}
