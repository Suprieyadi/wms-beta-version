/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.modules.core.services.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wms.wf.modules.core.models.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {
}
