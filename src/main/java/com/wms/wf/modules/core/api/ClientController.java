/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.modules.core.api;

import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.wms.wf.lib._BaseApi;
import com.wms.wf.modules.core.models.Client;
import com.wms.wf.modules.core.services.ClientService;
import com.wms.wf.modules.oauth.models.OauthApprovals;
import com.wms.wf.modules.oauth.models.OauthClientDetails;
import com.wms.wf.modules.oauth.services.OauthService;

import io.swagger.annotations.Api;

import java.util.Collection;

@Api(value = "Registered Applications API")
@RestController
@RequestMapping({"/api/core/clients", "/api/v1/core/clients"})
public class ClientController extends _BaseApi<Client> {

	@Autowired
	private ClientService clientService;
	@Autowired
	private OauthService oauthService;

	@Override
	public Client post(@RequestBody Client entity) {
		if (entity.getDetails() == null) {
			entity.setDetails(new OauthClientDetails());
		}
		return this.clientService.saveClient(entity.getId(), entity.getName(), entity.getIcon(),
			entity.getDetails().getWebServerRedirectUri(),
			entity.getDetails().getScope()
		);
	}

	@Override
	public Client put(@PathVariable(name = "id") long id, @RequestBody Client entity) {
		if (entity.getDetails() == null) {
			entity.setDetails(new OauthClientDetails());
		}
		return this.clientService.saveClient(id, entity.getName(), entity.getIcon(),
			entity.getDetails().getWebServerRedirectUri(),
			entity.getDetails().getScope()
		);
	}

	@ApiOperation(value = "Get my approvals")
	@RequestMapping( value= "/myapprovals", method = RequestMethod.GET)
	public Collection<OauthApprovals> getMyClientApprovals() {
		return this.oauthService.getApprovalsByUser(getCurrentUser().getUsername());
	}
}
