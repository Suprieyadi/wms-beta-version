/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.modules.core.services;

public interface SecurityService {
	
	String findLoggedInUsername();
	void autologin(String username, String password);
}
