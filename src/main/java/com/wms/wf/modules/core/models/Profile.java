package com.wms.wf.modules.core.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Version;

import java.math.BigDecimal;

@Entity
@Table(name = "core_profile", indexes = { @Index(name = "core_profile_Index", columnList = "statusEnabled", unique = false) })
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Profile {

	@Column(name = "KdProfile")
	@Id
	@GeneratedValue
	private Integer kode;

	@Column(name = "NamaLengkap")
	@NotNull(message = "profile.namalengkap.notnull")
	private String namaLengkap;

	@Column(name = "ReportDisplay")
	@NotNull(message = "profile.reportDisplay.notnull")
	private String reportDisplay;

	@Column(name = "TglRegistrasi")
	private Long tglRegistrasi;

	@Column(name = "KdLevelTingkat")
	private Integer kdLevelTingkat;

	@Column(name = "KdAlamat")
	private Integer kdAlamat;

	@Column(name = "GambarLogo")
	private String gambarLogo;

	@Column(name = "NPWP")
	private String nPWP;

	@Column(name = "NoPKP")
	private String noPKP;

	@Column(name = "KdAccount")
	private Integer kdAccount;

	@Column(name = "KdJenisProfile")
	@NotNull(message = "profile.kdjenisprofile.notnull")
	private Integer kdJenisProfile;
	
	@Column(name = "KdProfileHead")
	private Integer kdProfileHead;

	@Column(name = "KdNegara")
	private Integer kdNegara;

	@Column(name = "KdMataUang")
	private Integer kdMataUang;

	@Column(name = "KodeExternal")
	private String kodeExternal;

	@Column(name = "NamaExternal")
	private String namaExternal;

	@Column(name = "KdDepartemen")
	private String kdDepartemen;

	@Column(name = "Hostname1")
	private String hostname1;
	
	@Column(name = "Hostname2")
	private String hostname2; 
	
	@Column(name = "StatusEnabled")
	private Boolean statusEnabled;

	@Column(name = "NoRec")
	@NotNull(message = "NoRec Harus Diisi")
	private String noRec;

	@Version
	@Column(name = "version")
	@NotNull(message = "Version Harus Di Isi")
	private Integer version;

}
