/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.modules.core.api;

import io.swagger.annotations.Api;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wms.wf.ApplicationContext;
import com.wms.wf.lib._BaseApi;
import com.wms.wf.modules.core.models.UserProfile;

@Api(value = "User Profile API")
@RestController
@RequestMapping({"/api/core/userProfiles", "/api/v1/core/userProfiles"})
public class UserProfileController extends _BaseApi<UserProfile> {

    @Override
    public UserProfile post(@RequestBody UserProfile entity) {
        entity.setUserId(ApplicationContext.getCurrentUser().getId());
        return super.post(entity);
    }
}
