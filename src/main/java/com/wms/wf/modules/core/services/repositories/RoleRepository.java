/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.modules.core.services.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.wms.wf.modules.core.models.Role;

public interface RoleRepository extends JpaRepository<Role, Long>  {
	Role findByName(String name);

	@Modifying
	@Transactional
	@Query(value = "UPDATE core_role SET menu_ids = :menuIds WHERE id = :roleId", nativeQuery = true)
    void updateMenus(@Param("roleId") long roleId, @Param("menuIds") String menuIds);

	@Query(value = "SELECT COUNT(1) FROM core_user_role WHERE role_id = :roleId", nativeQuery = true)
	int countByRoleId(@Param("roleId") long roleId);
}
