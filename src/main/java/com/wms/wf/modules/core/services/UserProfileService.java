/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.modules.core.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wms.wf.lib._BaseService;
import com.wms.wf.modules.core.models.UserProfile;

@Service
@Transactional
public class UserProfileService  extends _BaseService<UserProfile> {

}
