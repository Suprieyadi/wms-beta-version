/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.modules.cms.api;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.wms.wf.exceptions.DisabledFeatureException;
import com.wms.wf.exceptions.NoResourceFoundException;
import com.wms.wf.lib._BaseApi;
import com.wms.wf.modules.cms.models.Page;
import com.wms.wf.modules.cms.services.PageService;
import com.wms.wf.modules.core.models.File;

import io.swagger.annotations.Api;

@Api(value = "Page API")
@RestController
@RequestMapping({"/api/cms/pages", "/api/v1/cms/pages"})
public class PageController extends _BaseApi<Page> {

	@Autowired
	PageService pageService;

	@Override
	public Page get(@PathVariable(name = "id") long id) throws NoResourceFoundException {
		Page page = this.pageService.getByChannelId(id);
		if (page == null) {
			throw new NoResourceFoundException();
		}
		return page;
	}

	@Override
	public File upload(MultipartFile file, HttpServletRequest request) throws IllegalStateException, IOException, DisabledFeatureException {
		return super.upload(file, request);
	}

}
