/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.config;

public class Constant {

	public static final String SESSION_USER_KEY = "USER";
	
	public static final int CLIENT_ID_LEN = 10;
	public static final int CLIENT_SECRET_LEN = 30;
	public static final int CLIENT_TOKEN_EXPIRE_IN = 10000;
}
