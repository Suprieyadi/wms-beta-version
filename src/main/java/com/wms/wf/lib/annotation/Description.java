/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.lib.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Description {
	public String value() default "";
}
