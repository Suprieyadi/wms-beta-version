/*******************************************************************************
 * Copyright (C) 2018 http://www.exsumoxor.com
 * Created by eX sumo Xor
 ******************************************************************************/
package com.wms.wf.lib;

public enum FileType {
	UNKNOWN,
	TEXT,
	IMAGE,
	AUDIO,
	VIDEO,
	ZIP
}
